## 故障演练之JVM垃圾回收

---

# 背景

故障演练期间，模拟故障注入，服务使用Elasticsearch进行兜底，瞬间引发大批接口超时。

---

___

<section>
  <img width=50% data-src="https://bitbucket.org/wenfengsui/slides/raw/0bfeca30ec54b3bccbafd1b786da0f980c8d8ef5/images/%20%E5%A4%A7%E6%89%B9%E6%8E%A5%E5%8F%A3%E8%B6%85%E6%97%B6.png">

  <img width=50% data-src="https://bitbucket.org/wenfengsui/slides/raw/0bfeca30ec54b3bccbafd1b786da0f980c8d8ef5/images/%20%E5%A4%A7%E6%89%B9%E6%8E%A5%E5%8F%A3%E8%B6%85%E6%97%B6.png">
</section>

---

## This is a forth title
<!-- .slide: data-background="#f70000" data-transition="page" -->

You can add slide attributes like above.
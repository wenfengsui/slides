---
customTheme : "swf"
transition: "slide"
highlightTheme: "monokai"
slideNumber: false
title: "Lombok Features"
---

# Lombok Features

author by suiwenfeng

---

## Stable Features

- **val**
Finally! Hassle-free final local variables.

- **var**
Mutably! Hassle-free local variables.

- **@NonNull**
How I learned to stop worrying and love the NullPointerException.

- **@Cleanup**
Automatic resource management: Call your close() methods safely with no hassle.

- **@Getter/@Setter**
Never write public int getFoo() {return foo;} again.

---

## Stable Features

- **@ToString**
No need to start a debugger to see your fields: Just let lombok generate a toString for you!

- **@EqualsAndHashCode**
Equality made easy: Generates hashCode and equals implementations from the fields of your object..

- **@NoArgsConstructor, @RequiredArgsConstructor and @AllArgsConstructor**
Constructors made to order: Generates constructors that take no arguments, one argument per final / non-nullfield, or one argument for every field.

- **@Data**
All together now: A shortcut for @ToString, @EqualsAndHashCode, @Getter on all fields, and @Setter on all non-final fields, and @RequiredArgsConstructor!

- **@Value**
Immutable classes made very easy.

---

## Stable Features

- **@Builder**
... and Bob's your uncle: No-hassle fancy-pants APIs for object creation!

- **@SneakyThrows**
To boldly throw checked exceptions where no one has thrown them before!

- **@Synchronized**
synchronized done right: Don't expose your locks.

- **@With**
Immutable 'setters' - methods that create a clone but with one changed field.

- **@Getter(lazy=true)**
Laziness is a virtue!

- **@Log**
Captain's Log, stardate 24435.7: "What was that line again?"

---

### val

You can use val as the type of a local variable declaration instead of actually writing the type. val is actually a 'type' of sorts, and exists as a real class in the lombok package.

```
import java.util.ArrayList;
import java.util.HashMap;
import lombok.val;

public class ValExample {
  public String example() {
    val example = new ArrayList<String>();
    example.add("Hello, World!");
    val foo = example.get(0);
    return foo.toLowerCase();
  }
  
  public void example2() {
    val map = new HashMap<Integer, String>();
    map.put(0, "zero");
    map.put(5, "five");
    for (val entry : map.entrySet()) {
      System.out.printf("%d: %s\n", entry.getKey(), entry.getValue());
    }
  }
}
```

---

### var

var works exactly like val, except the local variable is not marked as final.

---

### @NonNull

You can use @NonNull on the parameter of a method or constructor to have lombok generate a null-check statement for you.

```
import lombok.NonNull;

public class NonNullExample extends Something {
  private String name;
  
  public NonNullExample(@NonNull Person person) {
    super("Hello");
    this.name = person.getName();
  }
}
```

---

### @Cleanup

You can use @Cleanup to ensure a given resource is automatically cleaned up before the code execution path exits your current scope.

```
import lombok.Cleanup;
import java.io.*;

public class CleanupExample {
  public static void main(String[] args) throws IOException {
    @Cleanup InputStream in = new FileInputStream(args[0]);
    @Cleanup OutputStream out = new FileOutputStream(args[1]);
    byte[] b = new byte[10000];
    while (true) {
      int r = in.read(b);
      if (r == -1) break;
      out.write(b, 0, r);
    }
  }
}
```

---

### @Getter and @Setter

You can annotate any field with @Getter and/or @Setter, to let lombok generate the default getter/setter automatically.

```
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

public class GetterSetterExample {
  /**
   * Age of the person. Water is wet.
   * 
   * @param age New value for this person's age. Sky is blue.
   * @return The current value of this person's age. Circles are round.
   */
  @Getter @Setter private int age = 10;
  
  /**
   * Name of the person.
   * -- SETTER --
   * Changes the name of this person.
   * 
   * @param name The new value.
   */
  @Setter(AccessLevel.PROTECTED) private String name;
  
  @Override public String toString() {
    return String.format("%s (age: %d)", name, age);
  }
}
```

---

### @ToString

Any class definition may be annotated with @ToString to let lombok generate an implementation of the toString() method. 

```
import lombok.ToString;

@ToString
public class ToStringExample {
  private static final int STATIC_VAR = 10;
  private String name;
  private Shape shape = new Square(5, 10);
  private String[] tags;
  @ToString.Exclude private int id;
  
  public String getName() {
    return this.name;
  }
  
  @ToString(callSuper=true, includeFieldNames=true)
  public static class Square extends Shape {
    private final int width, height;
    
    public Square(int width, int height) {
      this.width = width;
      this.height = height;
    }
  }
}
```

---

### @EqualsAndHashCode

Any class definition may be annotated with @EqualsAndHashCode to let lombok generate implementations of the equals(Object other) and hashCode() methods. 

```
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class EqualsAndHashCodeExample {
  private transient int transientVar = 10;
  private String name;
  private double score;
  @EqualsAndHashCode.Exclude private Shape shape = new Square(5, 10);
  private String[] tags;
  @EqualsAndHashCode.Exclude private int id;
  
  public String getName() {
    return this.name;
  }
  
  @EqualsAndHashCode(callSuper=true)
  public static class Square extends Shape {
    private final int width, height;
    
    public Square(int width, int height) {
      this.width = width;
      this.height = height;
    }
  }
}
```

---

### @NoArgsConstructor, @RequiredArgsConstructor, @AllArgsConstructor

Constructors made to order: Generates constructors that take no arguments, one argument per final / non-null field, or one argument for every field.

```
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.NonNull;

@RequiredArgsConstructor(staticName = "of")
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class ConstructorExample<T> {
  private int x, y;
  @NonNull private T description;
  
  @NoArgsConstructor
  public static class NoArgsExample {
    @NonNull private String field;
  }
}
```

---

### @Data

All together now: A shortcut for @ToString, @EqualsAndHashCode, @Getter on all fields, @Setter on all non-final fields, and @RequiredArgsConstructor!

```
import lombok.AccessLevel;
import lombok.Setter;
import lombok.Data;
import lombok.ToString;

@Data public class DataExample {
  private final String name;
  @Setter(AccessLevel.PACKAGE) private int age;
  private double score;
  private String[] tags;
  
  @ToString(includeFieldNames=true)
  @Data(staticConstructor="of")
  public static class Exercise<T> {
    private final String name;
    private final T value;
  }
}
```

---

### @Value

@Value is the immutable variant of @Data; all fields are made private and final by default, and setters are not generated. 

```
import lombok.AccessLevel;
import lombok.experimental.NonFinal;
import lombok.experimental.Value;
import lombok.experimental.Wither;
import lombok.ToString;

@Value public class ValueExample {
  String name;
  @Wither(AccessLevel.PACKAGE) @NonFinal int age;
  double score;
  protected String[] tags;
  
  @ToString(includeFieldNames=true)
  @Value(staticConstructor="of")
  public static class Exercise<T> {
    String name;
    T value;
  }
}
```

---

### @Builder, @Singular

The @Builder annotation produces complex builder APIs for your classes.
if annotating a class with @Builder with the @Singular annotation, lombok will treat that builder node as a collection, and it generates 2 'adder' methods instead of a 'setter' method.

```
@Value @Builder
@JsonDeserialize(builder = JacksonExample.JacksonExampleBuilder.class)
public class JacksonExample {
	@Singular private List<Foo> foos;
	
	@JsonPOJOBuilder(withPrefix = "")
	public static class JacksonExampleBuilder implements JacksonExampleBuilderMeta {
	}
	
	private interface JacksonExampleBuilderMeta {
		@JsonDeserialize(contentAs = FooImpl.class) JacksonExampleBuilder foos(List<? extends Foo> foos)
	}
}
```

---

### @SneakyThrows

@SneakyThrows can be used to sneakily throw checked exceptions without actually declaring this in your method's throws clause.

```
import lombok.SneakyThrows;

public class SneakyThrowsExample implements Runnable {
  @SneakyThrows(UnsupportedEncodingException.class)
  public String utf8ToString(byte[] bytes) {
    return new String(bytes, "UTF-8");
  }
  
  @SneakyThrows
  public void run() {
    throw new Throwable();
  }
}
```

---

### @Synchronized

@Synchronized is a safer variant of the synchronized method modifier.
Locking on this or your own class object can have unfortunate side-effects.

```
import lombok.Synchronized;

public class SynchronizedExample {
  private final Object readLock = new Object();
  
  @Synchronized
  public static void hello() {
    System.out.println("world");
  }
  
  @Synchronized
  public int answerToLife() {
    return 42;
  }
  
  @Synchronized("readLock")
  public void foo() {
    System.out.println("bar");
  }
}
```

---

### @With

Immutable 'setters' - methods that create a clone but with one changed field.

```
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.With;

public class WithExample {
  @With(AccessLevel.PROTECTED) @NonNull private final String name;
  @With private final int age;
  
  public WithExample(String name, int age) {
    if (name == null) throw new NullPointerException();
    this.name = name;
    this.age = age;
  }
}
```

---

### @Getter(lazy=true)

You can let lombok generate a getter which will calculate a value once, the first time this getter is called,

```
import lombok.Getter;

public class GetterLazyExample {
  @Getter(lazy=true) private final double[] cached = expensive();
  
  private double[] expensive() {
    double[] result = new double[1000000];
    for (int i = 0; i < result.length; i++) {
      result[i] = Math.asin(i);
    }
    return result;
  }
}
```

---

### @Log

You put the variant of @Log on your class (whichever one applies to the logging system you use); you then have a static final log field.

```
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

@Log
public class LogExample {
  
  public static void main(String... args) {
    log.severe("Something's wrong here");
  }
}

@Slf4j
public class LogExampleOther {
  
  public static void main(String... args) {
    log.error("Something else is wrong here");
  }
}
```

